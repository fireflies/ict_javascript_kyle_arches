@extends('layouts.app')

@section('content')
<div id="page">
  <div id="header">
    <div> <a href="#"><img src="images/logo.gif" alt=""></a> </div>
    <ul>
            <li class="first"><a href="/home">Home</a></li>
            <li class="/add"><a href="/">Recipes</a></li>
            <li><a href="/about">About</a></li>
            <li><a href="/blog">Blog</a></li>
            <li class="selected" class="last"><a href="/create">Create</a></li>
    </ul>
  </div>
  <div id="content">
        <div>
          <div class="aside">
            <div> <span>Create Delicious Recipe!!! </span>
              <ul>
              <li> <img src="images/icon.jpg"></li>
              </ul>
            </div>
            <div></div>
            <div></div>
          </div>
          <div>
              {!! Form::open(['action' => 'AddController@store', 'method' => 'adds', 'enctype' => 'multipart/form-data']) !!}
              <div class="form-group">
              {{Form::label('title','Recipe Name')}}
              {{Form::text('title','',['class' => 'form-control', 'placeholder'=>'Enter Name'])}}
              </div>
              <div class="form-group">
                  {{Form::label('description','Ingredients')}}
                  {{Form::textarea('description','',['class' => 'form-control', 'placeholder'=>'Add Description'])}}
              </div>   
              <div class="form-group">
                  {{form::file('cover_image')}}  
              </div>        
              {{Form::submit('Submit', ['class'=>'btn btn-primary'])}}
              {!! Form::close() !!}
            </div>
        </div>
    </div>
            <div id="footer">
                    <div>
                      <div>
                        <div>
                          <div class="first">
                         </div>
                          <div>
                          </div>
                          <div>
                          </div>
                        </div>
                      </div>
                      <p class="footnote">The Healthy Food Life Style</p>
                    </div>
                  </div>
                </div>
@endsection