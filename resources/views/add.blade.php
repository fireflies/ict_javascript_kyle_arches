@extends('layouts.app')

@section('content')
<div id="page">
  <div id="header">
    <div> <a href="#"><img src="images/logo.gif" alt=""></a> </div>
    <ul>
      <li class="first"><a href="/home">Home</a></li>
      <li class="selected"><a href="/">Recipes</a></li>
      <li><a href="/about">About</a></li>
      <li><a href="/blog">Blog</a></li>
      <li><a href="/create">Create</a></li>
    </ul>
  </div>
  <h2 style="float: right">Create your own now !!!
      <ol>
          <button class="button button5"><a href="/create">Add Recipe</a></button>
        </ol>
      </h2><br>
      <center><h1><span>Your All Recipe</span></h1> </center>
<table>
    <tr>
        <th>The Recipe</th>
        <th>Name</th>
        <th>Date Added</th>
      </tr>
      <div>
@if(count($adds) >  0)

@foreach($adds as $add)
<tr>
        
    <td><img style="width:20%" src="/storage/cover_images/{{$add->cover_image}}">
    <td><h3><a href="/add/{{$add->id}}">{{$add->title}}</a></h3>
    <td><small> On {{$add->created_at}}</small>
    </tr>
  </div>
    
      

@endforeach
{{$adds -> links()}}
</table>
@else
<p>No posts found</p>
@endif
</table>
<div id="footer">
    <div>
      <div>
        <ul>
          <li> <a href="#"><img src="images/baking-fruits.jpg" alt=""></a>
            <h2>Baking Fruits</h2>
        
          </li>
          <li> <a href="#"><img src="images/health-benefits.jpg" alt=""></a>
            <h2>Health Benefits</h2>
            
          </li>
          <li> <a href="#"><img src="images/vitamins.jpg" alt=""></a>
            <h2>Vitamins in them</h2>

          </li>
        </ul>
      </div>
      <p class="footnote">The Healthy Food Life Style</p>
    </div>
  </div>
</div>
@endsection