<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Add;
use Illuminate\Support\Facades\Storage;

class Addcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $adds = Add::orderBy('title','asc') -> paginate(5);
       return view('add') -> with('adds', $adds);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'title' => 'required',
        'description' =>'required',
        'cover_image' => 'image|nullable|max:1999'
       ]);

      if($request->hasFile('cover_image')){
          $filenameWithExt = $request->file('cover_image')->getClientOriginalName();
          $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
          $extension = $request->file('cover_image') -> getClientOriginalExtension();
          $fileNameToStore = $filename. '_'.time().','.$extension;
          $path = $request->file('cover_image')->storeAs('public/cover_images', $fileNameToStore);
      }else {
          $fileNameToStore = 'noimage.jpg';
      }

       $add = new Add;
       $add->title =$request->input('title');
       $add->description =$request->input('description');
       $add->cover_image = $fileNameToStore;
       $add->save();

       return redirect('/add')->with('success','Successfully Added a Recipe');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $add = Add::find($id);
        return view('show')->with('add',$add);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $add = Add::find($id);
        return view('edit')->with('add',$add);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' =>'required'
           ]);

           if($request->hasFile('cover_image')){
            $filenameWithExt = $request->file('cover_image')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('cover_image') -> getClientOriginalExtension();
            $fileNameToStore = $filename. '_'.time().'.'.$extension;
            $path = $request->file('cover_image')->storeAs('public/cover_images', $fileNameToStore);
        }
    
           $add = Add::find($id);
           $add->title =$request->input('title');
           $add->description =$request->input('description');
           if($request->hasFile('cover_image')){
            $add->cover_image = $fileNameToStore;
           }
           $add->save();
           return redirect('/add')->with('success','Successfully Updated Recipe');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)

    {
        $add = Add::find($id);

        if($add->cover_image != 'noimage.jpg'){
          Storage::delete('public/cover_images/'.$add->cover_image);
        }
        $add -> delete();
        return redirect('/add')->with('success','Recipe Removed');
    }
}
