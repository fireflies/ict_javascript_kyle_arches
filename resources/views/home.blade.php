@extends('layouts.app')

@section('content')
            <div id="page">
                    <div id="header">
                      <div> <a href="#"><img src="images/logo.gif" alt=""></a> </div>
                      <ul>
                        <li class="first current"><a href="/">Home</a></li>
                        <li><a href="/add">Recipes</a></li>
                        <li><a href="/about">About</a></li>
                        <li><a href="/blog">Blog</a></li>
                        <li><a href="/create">Create</a></li>
                      </ul>
                    </div>
                    <div id="content"> <span class="background"></span>
                        <div id="home">
                          <ul>
                            <li class="first"> <a href="#"><img src="images/vegetables.jpg" alt=""></a>
                              <p>Lorem ipsum dolor sit</p>
                            </li>
                            <li> <a href="#"><img src="images/pastries.jpg" alt=""></a>
                              <p>Mirum est notare quam</p>
                            </li>
                          </ul>
                          <div> <a href="#"><img src="images/strawberry-pastry.jpg" alt=""></a>
                            <h1><a href="#"><span>Secrets of</span> <br>
                              Healthy eating</a></h1>
                            <p>Eat when you are physically hungry. Listen to your body's signals. Before you eat, stop and ask yourself if you are really hungry or whether you are eating for reasons such as stress, boredom, habit or any others. Keeping an eating diary is a great way to help you determine your eating patterns.
                                Eat smaller serves. Eating 3 smaller meals with a couple of healthy snacks between is a much better way to stay satisfied longer and keep your metabolism working efficiently. Avoid skipping meals where possible as this can lead to overeating later on.
                                Eat and drink slowly and give yourself a chance to feel satisfied without feeling overfull. It also helps you actually taste the food you are eating and enjoy it.
                                Eating healthier foods is your choice. Try remembering how much better you feel after eating healthy as against the awful feeling of overeating unhealthy foods. The 80/20 rule is worth keeping in mind (eat healthy 80% of the time and leave the other 20% for those times where you just have to have it!). Have a positive attitude towards food. Rather than thinking of foods as "good or bad" think in terms of "everyday or occasional foods", or "80/20 foods". This will erase those guilty feelings you have about eating certain things.
                                Don't multitask while you eat. If you're reading, working or watching TV while you eat, you won't be paying attention to what's going into your mouth- and you won't enjoy it. Chew slowly and enjoy each mouthful.
                                Listen to your body's cravings. </p>
                          </div>
                        </div>
                      </div>
                      <div id="footer">
                        <div>
                          <div>
                            <ul>
                              <li> <a href="#"><img src="images/baking-fruits.jpg" alt=""></a>
                                <h2>Baking Fruits</h2>
                            
                              </li>
                              <li> <a href="#"><img src="images/health-benefits.jpg" alt=""></a>
                                <h2>Health Benefits</h2>
                                
                              </li>
                              <li> <a href="#"><img src="images/vitamins.jpg" alt=""></a>
                                <h2>Vitamins in them</h2>
              
                              </li>
                            </ul>
                          </div>
                          <p class="footnote">The Healthy Food Life Style</p>
                        </div>
                      </div>
                    </div>
        @endsection