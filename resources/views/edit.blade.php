@extends('layouts.app')

@section('content')
<div id="page">
  <div id="header">
    <div> <a href="#"><img src="images/logo.gif" alt=""></a> </div>
    <ul>
      <li class="first"><a href="/home">Home</a></li>
      <li class="selected"><a href="/">Recipes</a></li>
      <li><a href="/about">About</a></li>
      <li><a href="/blog">Blog</a></li>
      <li><a href="/create">Create</a></li>
    </ul>
  </div>
  <div id="content">
        <div>
          <div class="aside">
            <ul>
         <li class="first"><img style="width:100%" src="/storage/cover_images/{{$add->cover_image}}"><center>Try Now !!!</center ></li>
        </ul>
    </div>
    <div>
<h1>Edit Recipe</h1>
              {!! Form::open(['action' => ['AddController@update', $add->id], 'method' => 'adds', 'enctype' => 'multipart/form-data']) !!}
              <div>
              {{Form::label('title','Title')}}
              {{Form::text('title',$add->title,['class' => 'form-control', 'placeholder'=>'Title'])}}
              </div>
              <div>
                  {{Form::label('description','Description')}}
                  {{Form::textarea('description',$add->description,['class' => 'form-control', 'placeholder'=>'Description'])}}
              </div>      
              <div>
                    {{form::file('cover_image')}}  
                </div>      
              {{Form::hidden('_method', 'PUT')}}  
              {{Form::submit('Submit', ['class'=>'btn btn-primary'])}}
              {!! Form::close() !!}
              <div id="footer">
                    <div>
                      <div>
                        <div>
                          <div class="first">
                         </div>
                          <div>
                          </div>
                          <div>
                          </div>
                        </div>
                      </div>
                      <p class="footnote">The Healthy Food Life Style</p>
                    </div>
                  </div>
                </div>
@endsection
